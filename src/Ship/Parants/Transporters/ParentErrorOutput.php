<?php

namespace App\Ship\Parants\Transporters;

use App\Ship\Parants\Exceptions\ParentException;

abstract class ParentErrorOutput
{
    public int $code;
    public string $message;
    public \Throwable $exception;

    public static function createByParentException(ParentException $exception): self
    {
        $error = new self();

        $error->code = $exception->getCode();
        $error->message = $exception->getMessage();
        $error->exception = $exception;

        return $error;
    }
}
