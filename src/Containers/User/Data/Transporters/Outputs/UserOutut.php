<?php

namespace App\Containers\User\Data\Transporters\Outputs;

use App\Containers\User\Data\Models\UserModel;
use App\Ship\Parants\Transporters\ParentSuccessOutput;

class UserOutut extends ParentSuccessOutput
{
    public int $id;
    public string $login;
    public ?string $email;
    
    public static function createByModel(UserModel $model): self
    {
        $output = new self();

        $output->id = $model->id;
        $output->login = $model->login;
        $output->email = $model->email;

        return $output;
    }
}
