<?php

declare(strict_types=1);

namespace App\Containers\User\Exceptions;

use App\Ship\Parants\Exceptions\ParentException;

class UserPasswordNotValidException extends ParentException
{
    private const CODE = 4;

    public function __construct(?string $message = null)
    {
        parent::__construct($message ?? 'Не валидный парль', $this->code());
    }

    private function code()
    {
        return (int)('100'. self::CODE);
    }
}

