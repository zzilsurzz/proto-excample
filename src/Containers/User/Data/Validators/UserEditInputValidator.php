<?php

namespace App\Containers\User\Data\Validators;

use App\Containers\User\Data\Models\UserModel;
use App\Containers\User\Data\Transporters\Inputs\UserEditInput;

class UserEditInputValidator extends UserValidator
{
    public function run(UserEditInput $input, UserModel $user)
    {   
        if ($input->login) {
            $this->loginValidation($input->login);
        }

        if ($input->password) {
            $this->passwordValidation($input->password);
        }

        if ($input->email) {
            $this->emailValidation($input->email);
        }
        
        if ($input->login !== null && $input->login !== $user->login) {
            $this->loginUniqueValidation($input->login);
        }
    }
}
