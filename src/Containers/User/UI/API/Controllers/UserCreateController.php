<?php

namespace App\Containers\User\UI\API\Controllers;

use App\Containers\User\Actions\UserCreateAction;
use App\Containers\User\Data\Transporters\Inputs\UserCreateInput;
use App\Containers\User\UI\API\Requests\UserCreateRequest;
use App\Containers\User\UI\API\Transformers\UserTransformer;
use App\Ship\Parants\Controllers\ParentController;

class UserCreateController extends ParentController
{
    private $action;
    private $transformer;
    
    public function __construct(UserCreateAction $action, UserTransformer $transformer)
    {
        $this->action = $action;
        $this->transformer = $transformer;    
    }

    public function run(UserCreateRequest $request)
    {
        $input = new UserCreateInput();

        $input->login = $request->getLogin();
        $input->password = $request->getPassword();
        $input->email = $request->getEmail();

        $userOutput = $this->action->run($input);

        return $this->transformer->transform($userOutput);
    }
}
