<?php

namespace App\Containers\User\Data\Repositoryes;

use App\Containers\User\Data\Models\UserModel;
use App\Ship\Parants\Repositories\ParentRepository;

class UserRepository extends ParentRepository
{
    protected function getTableName(): string
    {
        return 'User';
    }

    protected function getModelClassName(): string
    {
        return UserModel::class;
    }

    public function getByLogin(string $login): ?UserModel
    {
        //
    }
}
