<?php

declare(strict_types=1);

namespace App\Containers\User\Exceptions;

use App\Ship\Parants\Exceptions\ParentException;

class UserEmailNotValidException extends ParentException
{
    private const CODE = 5;

    public function __construct(?string $message = null)
    {
        parent::__construct($message ?? 'Не валидный email', $this->code());
    }

    private function code()
    {
        return (int)('100'. self::CODE);
    }
}
