<?php

namespace App\Containers\User\Data\Transporters\Inputs;

class UserCreateInput
{
    public string $login;
    public string $password;
    public ?string $email = null;
}
