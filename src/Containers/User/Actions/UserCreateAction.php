<?php

namespace App\Containers\User\Actions;

use App\Containers\User\Data\Models\UserModel;
use App\Containers\User\Data\Repositoryes\UserRepository;
use App\Containers\User\Data\Transporters\Inputs\UserCreateInput;
use App\Containers\User\Data\Transporters\Outputs\UserOutut;
use App\Containers\User\Data\Validators\UserCreateInputValidator;
use App\Ship\Parants\Actions\ParentAction;

class UserCreateAction extends ParentAction
{
    private UserRepository $repository;
    private UserCreateInputValidator $validator;
    
    public function __construct(UserRepository $repository, UserCreateInputValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;    
    }

    public function run(UserCreateInput $input)
    {
        
        $this->validator->run($input);
        
        $user = new UserModel();

        $user->login = $input->login;
        $user->password = $input->password;
        $user->email = $input->email ?: null;

        $this->repository->save($user);

        return UserOutut::createByModel($user);
    }
}
