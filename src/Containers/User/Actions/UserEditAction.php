<?php

namespace App\Containers\User\Actions;

use App\Containers\User\Data\Models\UserModel;
use App\Containers\User\Data\Repositoryes\UserRepository;
use App\Containers\User\Data\Transporters\Inputs\UserEditInput;
use App\Containers\User\Data\Transporters\Outputs\UserErrorOutput;
use App\Containers\User\Data\Transporters\Outputs\UserOutut;
use App\Containers\User\Data\Validators\UserEditInputValidator;
use App\Containers\User\Exceptions\UserNotFountException;
use App\Ship\Parants\Actions\ParentAction;
use App\Ship\Parants\Exceptions\ParentException;

class UserEditAction extends ParentAction
{   
    private UserRepository $userRepository;
    private UserEditInputValidator $validator;

    public function __construct(UserRepository $userRepository, UserEditInputValidator $validator)
    {
        $this->userRepository = $userRepository;
        $this->validator = $validator;
    }

    public function run(UserEditInput $input)
    {
        try {
            return UserOutut::createByModel($this->edit($input));
        } catch (ParentException $e) {
            return UserErrorOutput::createByParentException($e);
        }
    }

    private function edit(UserEditInput $input)
    {
        /**
         * @var UserModel|null;
         */
        $userModel = $this->userRepository->get($input->id);

        if ($userModel === null) {
            throw new UserNotFountException();
        }

        $this->validator->run($input, $userModel);

        if ($input->login) {
            $userModel->login = $input->login;
        }

        if ($input->password) {
            $userModel->password = $input->password;
        }
        
        if ($input->email !== null) {
            $userModel->email = $input->email ?: null;
        }

        $this->userRepository->save($userModel);

        return $userModel;
    }
}
