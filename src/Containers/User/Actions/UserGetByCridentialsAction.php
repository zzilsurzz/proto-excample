<?php

namespace App\Containers\User\Actions;

use App\Containers\User\Data\Repositoryes\UserRepository;
use App\Containers\User\Data\Transporters\Inputs\UserCridentialsInput;
use App\Containers\User\Data\Transporters\Outputs\UserErrorOutput;
use App\Containers\User\Data\Transporters\Outputs\UserOutut;
use App\Ship\Parants\Actions\ParentAction;
use App\Ship\Parants\Exceptions\ParentException;

class UserGetByCridentialsAction extends ParentAction
{
    private UserRepository $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(UserCridentialsInput $input)
    {
        try {
            return UserOutut::createByModel($this->get($input));
        } catch (ParentException $e) {
            return UserErrorOutput::createByParentException($e);
        }
    }

    private function get(UserCridentialsInput $input)
    {
        $user = $this->repository->getByLogin($input->login);

        if (!$user) {
            throw new \Exception('Пользователь с таким логином не найден');
        }

        if ($user->password !== $input->password) {
            throw new \Exception('Неверный пароль');
        }

        return $user;
    } 
}
