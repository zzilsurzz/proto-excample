<?php

namespace App\Containers\User\Data\Validators;

use App\Containers\User\Exceptions\UserEmailNotValidException;
use App\Containers\User\Exceptions\UserLoginNotUniqueException;
use App\Containers\User\Exceptions\UserLoginNotValidException;
use App\Containers\User\Exceptions\UserPasswordNotValidException;
use App\Ship\Parants\Validators\ParentValidator;

abstract class UserValidator extends ParentValidator
{
    protected function loginValidation($login): void
    {
        if (strlen($login) < 3) {
            throw new UserLoginNotValidException('короткий логин');
        }
    }

    protected function passwordValidation($password): void
    {
        if (strlen($password) < 6) {
            throw new UserPasswordNotValidException('короткий пароль');
        }

        if (preg_match("/@/", $password) !== false) {
            throw new UserPasswordNotValidException('запрещенные символы в пароле');
        }
    }

    protected function emailValidation(string $email): void
    {
        if (!$this->isEmail($email)) {
            throw new UserEmailNotValidException();
        }
    }

    protected function loginUniqueValidation($login): void
    {
        // TO-DO
        if (false) {
            throw new UserLoginNotUniqueException();
        }
    }
}
