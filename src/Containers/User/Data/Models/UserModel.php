<?php

namespace App\Containers\User\Data\Models;

use App\Ship\Parants\Models\ParentModel;

class UserModel extends ParentModel
{
    public string $login;
    public string $password;
    public ?string $email = null;
}
