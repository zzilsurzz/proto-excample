<?php

declare(strict_types=1);

namespace App\Ship\Parants\Validators;

abstract class ParentValidator
{
    protected function isEmail(string $email): bool
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }
}
