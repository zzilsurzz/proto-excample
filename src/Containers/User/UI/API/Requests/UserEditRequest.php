<?php

namespace App\Containers\User\UI\API\Requests;

use App\Ship\Parants\Requests\ParentRequest;

class UserEditRequest extends ParentRequest
{
    private $_POST;

    public function getLogin()
    {
        return $this->_POST['login'];
    }

    public function getPassword()
    {
        return $this->_POST['password'];
    }

    public function getEmail()
    {
        return $this->_POST['email'] ?? null;
    }
}
