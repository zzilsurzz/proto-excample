<?php

namespace App\Containers\User\Exceptions;

use App\Ship\Parants\Exceptions\ParentException;

class UserNotFountException extends ParentException
{
    private const CODE = 1;

    public function __construct()
    {
        parent::__construct('Пользователь не найден', $this->code());
    }

    private function code()
    {
        return (int)('100'. self::CODE);
    }
}
