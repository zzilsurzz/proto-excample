<?php

namespace App\Containers\User\Data\Validators;

use App\Containers\User\Data\Transporters\Inputs\UserCreateInput;

class UserCreateInputValidator extends UserValidator
{
    public function run(UserCreateInput $input)
    {
        $this->loginValidation($input->login);
        $this->passwordValidation($input->password);
        
        if ($input->email) {
            $this->emailValidation($input->email);
        }

        $this->loginUniqueValidation($input->login);
    }
}
