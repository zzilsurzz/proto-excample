<?php

namespace App\Containers\User\UI\API\Controllers;

use App\Containers\User\Actions\UserEditAction;
use App\Containers\User\Data\Transporters\Inputs\UserEditInput;
use App\Containers\User\UI\API\Requests\UserEditRequest;
use App\Containers\User\UI\API\Transformers\UserTransformer;
use App\Ship\Parants\Controllers\ParentController;

class UserEditController extends ParentController
{
    private $action;
    private $transformer;
    
    public function __construct(UserEditAction $action, UserTransformer $transformer)
    {
        $this->action = $action;
        $this->transformer = $transformer;    
    }

    public function run(UserEditRequest $request)
    {
        $input = new UserEditInput();

        $input->login = $request->getLogin();
        $input->password = $request->getPassword();
        $input->email = $request->getEmail();
        
        $userOutput = $this->action->run($input);

        return $this->transformer->transform($userOutput);
    }
}
