<?php

namespace App\Containers\User\UI\CLI\Commands;

use App\Containers\User\Actions\UserCreateAction;
use App\Containers\User\Data\Transporters\Inputs\UserCreateInput;
use App\Ship\Parants\Commands\ParentCommand;

class UserCreateCommand extends ParentCommand
{
    private $action;

    public function __construct(UserCreateAction $action)
    {
        $this->action = $action;
    }
    
    public function run()
    {
        $login = $this->ask('Login');
        $password = $this->askProtected('Password');

        $input = new UserCreateInput();

        $input->login = $login;
        $input->password = $password;

        $this->action->run($input);

        return 'Пользователь создан';
    }
}
