<?php

namespace App\Containers\User\Services;

use App\Containers\User\Actions\UserCreateAction;
use App\Containers\User\Actions\UserEditAction;
use App\Containers\User\Actions\UserGetAction;
use App\Containers\User\Actions\UserGetByCridentialsAction;
use App\Containers\User\Data\Transporters\Inputs\UserCreateInput;
use App\Containers\User\Data\Transporters\Inputs\UserCridentialsInput;
use App\Containers\User\Data\Transporters\Inputs\UserEditInput;
use App\Containers\User\Data\Transporters\Outputs\UserErrorOutput;
use App\Containers\User\Data\Transporters\Outputs\UserOutut;
use App\Ship\Parants\Services\ParentService;

class UserService extends ParentService
{
    private DI $container;

    public function __construct(DI $container)
    {
        $this->container = $container;    
    }

    public function get(int $id): UserOutut|UserErrorOutput
    {
        return $this->container->get(UserGetAction::class)->run($id);
    }

    public function create(UserCreateInput $input): UserOutut|UserErrorOutput
    {
        return $this->container->get(UserCreateAction::class)->run($input);
    }

    public function edit(UserEditInput $input): UserOutut|UserErrorOutput
    {
        return $this->container->get(UserEditAction::class)->run($input);
    }

    public function getByCridentials(UserCridentialsInput $input): UserOutut|UserErrorOutput
    {
        return $this->container->get(UserGetByCridentialsAction::class)->run($input);
    }
}
