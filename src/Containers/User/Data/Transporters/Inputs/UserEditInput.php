<?php

namespace App\Containers\User\Data\Transporters\Inputs;

class UserEditInput
{
    public int $id;
    public ?string $login = null;
    public ?string $password = null;
    public ?string $email = null;
}
