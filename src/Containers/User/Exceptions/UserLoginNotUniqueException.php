<?php

declare(strict_types=1);

namespace App\Containers\User\Exceptions;

use App\Ship\Parants\Exceptions\ParentException;

class UserLoginNotUniqueException extends ParentException
{
    private const CODE = 3;

    public function __construct()
    {
        parent::__construct('Логин занят', $this->code());
    }

    private function code()
    {
        return (int)('100'. self::CODE);
    }
}