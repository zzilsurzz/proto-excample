<?php

namespace App\Containers\Auth\Data\Transporters\DTO;

class AccessTokenPayloadDTO
{
    public int $userId;
    public int $expIn;
}
