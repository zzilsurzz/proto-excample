<?php

namespace App\Ship\Parants\Repositories;

use App\Ship\Parants\Models\ParentModel;

abstract class ParentRepository
{
    private $db;

    abstract protected function getTableName(): string;
    abstract protected function getModelClassName(): string;

    public function save(ParentModel $model)
    {
        if (!$model->id) {
            $this->insert($model);
        } else {
            $this->update($model);
        }
    }

    public function get(int $id): ?ParentModel
    {
        $sql = sprintf("SELECT * FROM `%s` WHERE %s = %d", 
            $this->getTableName(),
            $this->mapPropertyToDb('id'),
            $id
        );

        if (!$row = $this->db->get_row($sql)) {
            return null;
        }

        return $this->mapRowToModel($row);
    }

    private function insert(ParentModel $model): void
    {
        $sql = sprintf("INSERT INTO `%s` (%s) VALUES (%s)", 
            $this->getTableName(), 
            ...$this->modelToInsert($model),
        );

        $this->db->query($sql);

        $model->id = $this->db->last_insert;
    }

    private function update(ParentModel $model): void
    {
        $sql = sprintf("UPDATE `%s` SET %s WHERE %s = %d", 
            $this->getTableName(), 
            $this->modelToUpdate($model),
            $this->mapPropertyToDb('id'),
            $model->id,
        );

        $this->db->query($sql);
    }

    private function modelToInsert(ParentModel $model): array
    {
        $fields = [];
        $values = [];

        foreach ($model as $prop => $value) {
            if ($prop === 'id') continue;

            $fields[] = $this->mapPropertyToDb($prop);
            $values[] = $this->mapValueToDb($value);
        }

        return [
            implode(',', $fields),
            implode(',', $values),
        ];
    }

    private function modelToUpdate(ParentModel $model): string
    {
        $set = [];

        foreach ($model as $prop => $value) {
            if ($prop === 'id') continue;

            $set[] = sprintf('%s = %s', $this->mapPropertyToDb($prop), $this->mapValueToDb($value));
        }

        return implode(',', $set);
    }

    private function mapPropertyToDb(string $property): string
    {
        return sprintf('%s.`%s`', $this->getTableName(), $property);
    }

    private function mapValueToDb(mixed $value): string
    {
        if ($value === null) {
            return 'NULL';
        }

        if (is_int($value)) {
            return "{$value}";
        }

        return "'{$value}'";
    }

    private function mapRowToModel(\stdClass $row): ParentModel
    {
        $model = $this->getModel();

        foreach ($row as $field => $value) {
            $model->$field = $value;
        }

        return $model;
    }

    private function getModel(): ParentModel
    {
        $class = $this->getModelClassName();

        if (is_a($class, ParentModel::class, true)) {
            throw new \Exception('модель должна наследоваться от ParentModel');
        }

        return new $class;
    }
}
