<?php

namespace App\Containers\User\Data\Transporters\Inputs;

use App\Ship\Parants\Transporters\ParentInput;

class UserCridentialsInput extends ParentInput
{
    public string $login;
    public string $password;
}
