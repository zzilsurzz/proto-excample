<?php

namespace App\Containers\Auth\Data\Transporters\Inputs;

use App\Ship\Parants\Transporters\ParentInput;

class AuthAuthentificationInput extends ParentInput
{
    public string $login;
    public string $password;
}
