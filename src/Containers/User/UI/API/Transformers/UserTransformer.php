<?php

namespace App\Containers\User\UI\API\Transformers;

use App\Containers\User\Data\Transporters\Outputs\UserOutut;
use App\Ship\Parants\Requests\ParentRequest;

class UserTransformer extends ParentRequest
{
    public function transform(UserOutut $userOutut)
    {
        return json_encode($userOutut);
    }
}
