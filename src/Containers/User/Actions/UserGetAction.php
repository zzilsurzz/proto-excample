<?php

namespace App\Containers\User\Actions;

use App\Containers\User\Data\Models\UserModel;
use App\Containers\User\Data\Repositoryes\UserRepository;
use App\Containers\User\Data\Transporters\Outputs\UserOutut;
use App\Containers\User\Exceptions\UserNotFountException;
use App\Ship\Parants\Actions\ParentAction;

class UserGetAction extends ParentAction
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;    
    }
    /**
     * @param string $id 
     */
    public function run($id)
    {
        /** @var UserModel|null */
        $user = $this->userRepository->get($id);

        if ($user === null) {
            throw new UserNotFountException();
        }

        return UserOutut::createByModel($user);
    }
}
