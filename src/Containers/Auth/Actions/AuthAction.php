<?php

namespace App\Containers\Auth\Actions;

use App\Containers\Auth\Data\Transporters\DTO\AccessTokenPayloadDTO;
use App\Containers\Auth\Data\Transporters\Inputs\AuthAuthentificationInput;
use App\Containers\Auth\Providers\UserProvider;
use App\Containers\User\Data\Transporters\Inputs\UserCridentialsInput;
use App\Containers\User\Data\Transporters\Outputs\UserErrorOutput;
use App\Containers\User\Data\Transporters\Outputs\UserOutut;
use App\Ship\Parants\Actions\ParentAction;

class AuthAction extends ParentAction
{
    private UserProvider $userProvider;

    public function __construct(
        UserProvider $userProvider,
    )
    {
    }

    public function run(AuthAuthentificationInput $input)
    {
        try {
            return $this->auth($input);
        } catch (\Exception $e) {

        }
    }

    private function auth(AuthAuthentificationInput $input)
    {
        $userInput = new UserCridentialsInput;

        $userInput->login = $input->login;
        $userInput->password = $input->password;

        $user = $this->userProvider->getByCridentials($userInput);

        if ($user instanceof UserErrorOutput) {
            throw new Exception('Неверные данные доступа');
        }

        return $this->tokenGenerate($user);
    }

    private function tokenGenerate(UserOutut $user): string
    {
        $accessToken = new AccessTokenPayloadDTO;

        $accessToken->userId = $user->id;
        $accessToken->expIn = microtime() + 60;

        return base64_encode(json_encode($accessToken));
    }
}
